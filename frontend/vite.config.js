import {defineConfig, loadEnv} from 'vite';
import vue from '@vitejs/plugin-vue';

export default defineConfig(({mode}) => {
    const env = loadEnv(mode, process.cwd(), '');
    const backendServerProxy = {
        target: `http://127.0.0.1:${env.TARGET_PORT ?? 8000}/`,
        changeOrigin: true,
        cookieDomainRewrite: {localhost: '127.0.0.1'},
    };
    const telegramBackendServerProxy = {
        target: `http://127.0.0.1:${env.TELEGRAM_TARGET_PORT ?? 3000}/`,
        changeOrigin: true,
        cookieDomainRewrite: {localhost: '127.0.0.1'},
    };

    return {
        plugins: [
            vue(),
        ],
        logLevel: 'info',
        server: {
            host: '127.0.0.1',
            port: env.FRONTEND_PORT ?? 5647,
            proxy: {
                '/api/telegram': telegramBackendServerProxy,
                '/api': backendServerProxy,
                '/admin': backendServerProxy,
                '/static': backendServerProxy,
                '/rest': backendServerProxy,
                '/flower': backendServerProxy,
                '/media': backendServerProxy,
            },
        },
    };
});
