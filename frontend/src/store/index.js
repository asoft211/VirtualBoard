import { createStore } from 'vuex';

import createPersistedState from 'vuex-persistedstate';

import project from './project';
import leftSidebar from './leftSidebar';

export default new createStore({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    project,
    leftSidebar,
  },
  plugins: [createPersistedState()],
});
