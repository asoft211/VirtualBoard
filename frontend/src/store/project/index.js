import axios from '../../axios';

export default {
  actions: {
    async loadProjectData({ commit }) {
      const { data } = await axios('/api/project-data/');
      commit('ADD_PROJECT_DATA', data);
    },
    addProjectData({ commit }, data) {
      commit('ADD_PROJECT_DATA', data);
    },
    changeLocale({ commit }, locale) {
      commit('CHANGE_LOCALE', locale);
    },
  },
  getters: {
    projectData(state) {
      return state.projectData;
    },
    locale(state) {
      return state.locale;
    },
  },
  mutations: {
    ADD_PROJECT_DATA(state, data) {
      state.projectData = data;
    },
    CHANGE_LOCALE(state, locale) {
      state.locale = locale;
      this._app.__VUE_I18N__.global.locale = locale;
    },
  },
  state: {
    projectData: {},
    locale: 'ru-RU',
  },
};
