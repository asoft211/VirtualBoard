export default {
  namespaced: true,
  actions: {
    onChangeIsCollapse({ commit }, state) {
      commit('COLLAPSE_STATE_CHANGE', state);
    },
  },
  getters: {
    collapse: (state) => state.isCollapse,
  },
  mutations: {
    COLLAPSE_STATE_CHANGE(state, data) {
      state.isCollapse = data;
    },
  },
  state: {
    isCollapse: true,
  },
};
