import WorkspaceRoutes from './workspace/routes.js';

export default [
  {
    path: '',
    name: 'workspace',
    component: () => import('./workspace/WorkspacePage.vue'),
    children: WorkspaceRoutes,
  },
];
