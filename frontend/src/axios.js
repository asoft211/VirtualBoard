import axios from 'axios';

axios.defaults.withCredentials = true;
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';
axios.defaults.xsrfCookieName = '_csrftoken';

axios.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response && error.response.data && error.response.data.location) {
      window.location = `${error.response.data.location}?next=${window.location}`;
      return error.response;
    }
    return Promise.reject(error);
  },
);

export default axios;
