export default {
  install: (app, options) => {
    app.config.globalProperties.$logger = (...args) => {
      console.error(...args);
    };
  },
};
