export default {
  install: (app) => {
    app.config.globalProperties.$modalForm = function (component, props) {
      const { modal } = this.$root.$refs;
      modal.component = component;
      modal.props = props;
      modal.show = true;
      return new Promise(
        (resolve, reject) => {
          modal.resolve = resolve;
          modal.reject = reject;
        },
      );
    };
  },
};
