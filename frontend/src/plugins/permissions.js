import axios from '../axios';

export default {
  install: (app, options) => {
    app.config.globalProperties.$hasPermission = async (permissionClassName) => {
      const params = { permissionClassName };
      const { data } = await axios.get('/api/permissions/check-permission/', { params });
      return !!data;
    };
  },
};
