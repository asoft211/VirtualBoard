import {createApp} from 'vue';
import './style.css';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
import ElementPlus from 'element-plus';
import App from './App.vue';
import router from './router.js';
import store from './store';
import 'element-plus/dist/index.css';
// region Plugins
import axios from './plugins/axios';
import modal from './plugins/modal';
import permissions from './plugins/permissions';



const app = createApp(App);
app.use(ElementPlus);
app.use(router);
app.use(store);
app.use(axios);
app.use(modal);
app.use(permissions);
// endregion

store._app = app;

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component);
}
app.mount('#app');
