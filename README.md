# VirtualBoard



## Для запуска фронтенада

- [ ] Установить в системе [Yarn](https://yarnpkg.com/)
- [ ] Установить в системе [Nvm Win](https://github.com/coreybutler/nvm-windows) или [Nvm Mac](https://osxdaily.com/2014/02/12/install-command-line-tools-mac-os-x/). [Универсальная инструкция](https://github.com/nvm-sh/nvm#installing-and-updating)
- [ ] Установить через nvm nodejs последней версии 
```
nvm install node
```
- [ ] Выполнить в терминале команды
```
cd frontend
yarn install
```
- [ ] Выполнить в терминале команды
```
yarn dev
```
Это можно автоматизировать. Пример авторизации в продуктов [JetBrains Pycharm](https://www.jetbrains.com/pycharm/). Можно WebStorm только для фронтенда он.
![img.png](img.png)
- [ ] Открыть в браузере http://127.0.0.1:5647/

